const BASE_URL = "https://634e6c704af5fdff3a5bb501.mockapi.io";

let phoneList = [];

const cartItem = "cartItem";

let dataJSON = localStorage.getItem(cartItem);
if (dataJSON) {
    let dataRaw = JSON.parse(dataJSON);
    phoneList = dataRaw.map((item) => {
        return new CartProduct(item.product, item.quantity);
    });
}
let saveLocalStorage = () => {
    let dataJSON = JSON.stringify(phoneList);
    localStorage.setItem(cartItem, dataJSON);
};

let fetchListProducts = () => {
    axios({
        url: `${BASE_URL}/phone`,
        method: "GET",
    })
        .then(function (item) {
            renderListProducts(item.data);
            console.log("item: ", item);
        })
        .catch(function (err) {
            console.log("err: ", err);
        });
};
fetchListProducts();

let filterProduct = () => {
    axios({
        url: `${BASE_URL}/phone`,
        method: "GET",
    })
        .then((item) => {
            filterList(item.data);
        })
        .catch((err) => {
            console.log("err: ", err);
        });
};

let addItem = (cartItem) => {
    let item = new PRODUCT(
        cartItem.id,
        cartItem.name,
        cartItem.price,
        cartItem.screen,
        cartItem.backCamera,
        cartItem.frontCamera,
        cartItem.img,
        cartItem.desc,
        cartItem.type
    );

    addProductToCart(item, phoneList);
    return phoneList;
};

let addProduct = (id) => {
    axios({
        url: `${BASE_URL}/phone/${id}`,
        method: "GET",
    })
        .then((res) => {
            let result = addItem(res.data);
            renderCart(result);
            saveLocalStorage();
            calculation();
            swal("Sản Phẩm Đã Được Thêm Vào Giỏ Hàng");
        })
        .catch((err) => {
            console.log("err: ", err);
            swal("Thêm Thất Bại");
        });
};

let clearCart = () => {
    swal("Đơn hàng đã được thanh toán", "Cám ơn quý khách", "success");
    phoneList = [];
    renderCart(phoneList);
    saveLocalStorage();
    calculation();
    document.getElementById("showcart").style.display = "none";
};

let calculation = () => {
    let cartIcon = document.getElementById("cartAmount");
    cartIcon.innerHTML = phoneList.map((x) => x.quantity).reduce((x, y) => x + y, 0);
};
calculation();
