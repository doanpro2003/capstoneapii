
let renderListProducts = (list) => {
    let contentHTML = "";
    list.forEach(function (item) {
        contentHTML += `
<div class="card col-md-3">
  <div class="card-header">
    <div class="name">${item.name}</div>
  </div>
  <div class="card-body">
   <img src="${item.img}" alt="" />
  </div>
  <div class="card-content">
    <div class="screen">
  <span>Screen:${item.screen} </span>
 </div>
 <div class="backcam">
  <span>backCamera:${item.backCamera} </span>
 </div>
 <div class="frontcam">
  <span>frontCamera:${item.frontCamera} </span>
 </div>
 <div class="desc"> ${item.desc}</div>
 </div>
  <div class="card-footer" style="align-items:center;">
  <div class="price"> Giá: $ ${item.price}</div>
  <button class=" btn btn-success" onclick="addProduct(${item.id})">Add</button>
  </div>
</div>
    `;
    });
    document.getElementById("cardContainer").innerHTML = contentHTML;
};


let filterList = (productList) => {
    let selectProduct = document.getElementById("productList");

    if (selectProduct.value === "All") {
        return renderListProducts(productList);
    } else {
        let product = productList.filter((item) => {
            return item.type === selectProduct.value;
        });
        renderListProducts(product);
    }
};


document.getElementById("showcart").style.display = "none";
let showcart = () => {
    var isCartShow = document.getElementById("showcart");
    if (isCartShow.style.display == "block") {
        isCartShow.style.display = "none";
    } else {
        isCartShow.style.display = "block";
    }
    renderCart(phoneList);
};


let addProductToCart = function (productCart, phoneList) {
    let quantity = 1;
    let product = new CartProduct(productCart, quantity);
    if (phoneList.length == 0) {
        phoneList.push(product);
    } else {
        for (let i = 0; i < phoneList.length; i++) {
            if (phoneList[i].product.id == productCart.id) {
                phoneList[i].quantity++;
                return;
            } else {
                let itemExist = phoneList.find((item) => {
                    return item.product.id == productCart.id;
                });
                if (itemExist == undefined) {
                    phoneList.push(product);
                    return;
                }
            }
        }
    }
};


let renderCart = (listCartProduct) => {
    let contentHTML = "";
    let total = 0;

    listCartProduct.forEach((item, index) => {
        total += itemTotalPrice(item.product.price, item.quantity);
        contentHTML += `
      
    <tr>
    <th scope="row" class="item-cart  justify-content-between ">
      <p >
        <img src="${item.product.img}" alt=""></p>
        <p >${item.product.name}</p>
    </td>
    <td>
        <span onclick="minusNumber(${index})"> <i class="fa fa-minus-circle"></i></span>
       
        <span class="col">${item.quantity}</span>
        
        <span onclick="plusNumber(${index})"><i class="fa fa-plus-circle"></i></span>
        
    </td>
        
    <td>
        <p class="col p-0">$ ${itemTotalPrice(
            item.product.price,
            item.quantity
        )}</p>
    </td>
    <td>
        <span onclick="deleteProduct(${index})">
        <i class="fa fa-trash"></i></span>
    </td> 
    </tr>
        `;
    });

    document.getElementById("phoneList").innerHTML = contentHTML;
    document.getElementById("priceTotal").innerHTML = `TỔNG TIỀN: $ ${total}`;
};


let plusNumber = (qty) => {
    phoneList[qty].quantity++;
    renderCart(phoneList);
    saveLocalStorage();
    calculation();
};


let minusNumber = (qty) => {
    if (phoneList[qty].quantity == 1) {
        phoneList.splice(qty, 1);
        renderCart(cart);
        saveLocalStorage();
        calculation();
    } else {
        phoneList[qty].quantity--;
        renderCart(phoneList);
        saveLocalStorage();
        calculation();
    }
};
let itemTotalPrice = (price, quantity) => {
    return price * quantity;
};

let deleteProduct = (index) => {
    phoneList.splice(index, 1);
    renderCart(phoneList);
    saveLocalStorage();
    calculation();
};
